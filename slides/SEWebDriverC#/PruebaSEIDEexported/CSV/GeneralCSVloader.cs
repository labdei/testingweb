﻿using System;
using System.Collections.Generic;
using System.IO;

namespace PruebaSEIDEexported.CSV
{
    public class GeneralCSVloader
    {
        public List<string> loadCsvFile(string filePath)
        {
            var reader = new StreamReader(File.OpenRead(filePath));
            List<string> searchList = new List<string>();
            while (!reader.EndOfStream)
            {
                var line = reader.ReadLine();
                searchList.Add(line);
            }
            return searchList;
        }
    }

    public class EnumerableCSVloader
    {
        StreamReader reader2;

        EnumerableCSVloader()
        { }
        EnumerableCSVloader(string filePath)
        {
            reader2 = new StreamReader(File.OpenRead(filePath));
        }
        public static IEnumerable<String[]> GetTestData()
        {
            using (StreamReader reader = new StreamReader(File.OpenRead(@"C:\Users\FCEFyN\Desktop\slenium\PruebaSEIDEexported\CSV\usuarios.csv")))
            {
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var values = line.Split(';');
                    yield return new[] { values[0], values[1] };
                }
            }
        }
    }

    public static class Servers
    {
        private static T getServers<T>() where T : new()
        {
            var servers = new T();
            return servers;
        }
        public static GeneralCSVloader general
        {
            get { return getServers<GeneralCSVloader>(); }
        }

    }
}
