using NUnit.Framework;
using System;

namespace PruebaSEIDEexported
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void Test1()
        {
            Assert.Pass();
        }
    }

    [TestFixture]
    public class PrimeService_IsPrimeShould
    {
        private PrimeService _primeService;

        [SetUp]
        public void SetUp()
        {
            _primeService = new PrimeService();
        }

        [Test]
        public void IsPrime_InputIs1_ReturnFalse()
        {
            var result = _primeService.greaterThan2(1);

            Assert.IsFalse(result, "1 should not be prime");
        }

        [TestCase(-1)]
        [TestCase(0)]
        [TestCase(1)]
        public void IsPrime_ParametrizedTest(int value)
        {
            var result = _primeService.greaterThan2(value);

            Assert.IsFalse(result, $"{value} should not be prime");
        }

        [Test]

        public void testCalculate()

        {

            Assert.AreEqual(100, PrimeService.calculate(10, 10, 0));

        }

        [TestCase(10, 10, 10, 90)]
        [TestCase(10, 10, 0, 100)]
        public void testCalculate(double price, int quantity, double discount, double expectedFinalAmount)

        {

            Assert.AreEqual(expectedFinalAmount, PrimeService.calculate(price, quantity, discount));

        }


        }
    

    public class PrimeService
    {
        public bool greaterThan2(int candidate)
        {
            if (candidate < 2)
            {
                return false;
            }
            throw new NotImplementedException("Please create a test first");
        }

        public static double calculate(double price, int quantity, double discount)

        {

            double totalPrice = price * quantity;

            double totalPriceWithDiscount = System.Math.Round(totalPrice - (totalPrice * discount / 100), 2);

            return totalPriceWithDiscount;

        }
    }
}
