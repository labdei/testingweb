// Generated by Selenium IDE
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Interactions;
using NUnit.Framework;
using SeleniumExtras.PageObjects;
using PruebaSEIDEexported;
using PruebaSEIDEexported.CSV;

[TestFixture]
public class LoginPlanoPOMTest {
  private IWebDriver driver;
  public IDictionary<string, object> vars {get; private set;}
  private IJavaScriptExecutor js;
  [SetUp]
  public void SetUp() {
    //driver = new FirefoxDriver();
    //js = (IJavaScriptExecutor)driver;
    //vars = new Dictionary<string, object>();
  }
  [TearDown]
  protected void TearDown() {
    //driver.Quit();
  }
  [Test]
  public void loginPlanoUsingPOM() {
    driver.Navigate().GoToUrl("http://asp.thedemosite.co.uk/");
    driver.FindElement(By.LinkText("4. Login")).Click();
    LoginPage p = new LoginPage();
    PageFactory.InitElements(driver,p);
    p.Login("user1", "1234");
    Assert.That(p.LoginResult(), Is.EqualTo(p.LogginFailedMessge()));
  }
    [Ignore("para resolver en el taller")]
    [Test, TestCaseSource(typeof(EnumerableCSVloader), "GetTestData")]
    public void loginPlanoUsingPOMCSVTestcaseSource(String user, String password)
    {
        driver.Navigate().GoToUrl("http://asp.thedemosite.co.uk/");
        driver.FindElement(By.LinkText("4. Login")).Click();
        LoginPage p = new LoginPage();
        PageFactory.InitElements(driver, p);
        p.Login(user, password);
        Assert.That(p.LoginResult(), Is.EqualTo(p.LogginFailedMessge()));
    }

    [Ignore("para resolver en el taller")]
    [Parallelizable(ParallelScope.All)]
    [Test, TestCaseSource(typeof(EnumerableCSVloader), "GetTestData")]
    public void loginPlanoUsingPOMMMCSVPARALELL(String user, String password)
    {
        IWebDriver driverPrivado = new FirefoxDriver();
        js = (IJavaScriptExecutor) driverPrivado;
        vars = new Dictionary<string, object>();
        driverPrivado.Navigate().GoToUrl("http://asp.thedemosite.co.uk/");
        driverPrivado.FindElement(By.LinkText("4. Login")).Click();
        LoginPage p = new LoginPage();
        PageFactory.InitElements(driverPrivado, p);
        p.Login(user, password);
        Assert.That(p.LoginResult(), Is.EqualTo(p.LogginFailedMessge()));
        driverPrivado.Quit();
    }
}
