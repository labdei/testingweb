﻿using SeleniumExtras.PageObjects;
using System;
using System.Collections.Generic;
using System.Text;

namespace PruebaSEIDEexported.Pages
{
    class Pages
    {
            private static T getPages<T>() where T : new()
            {
                var page = new T();
                PageFactory.InitElements(Browsers.getDriver, page);
                return page;
            }

    }
}
