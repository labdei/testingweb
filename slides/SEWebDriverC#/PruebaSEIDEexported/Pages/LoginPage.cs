﻿using System;
using System.Collections.Generic;
using System.Text;

//namespace PruebaSEIDEexported.Pages
//{
//    class contact
//    {
//    }
//}

using System.Linq;
using System.Threading.Tasks;
using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System.Threading;
using NUnit.Framework;
using System.IO;
using PruebaSEIDEexported.CSV;

namespace PruebaSEIDEexported
{
    class LoginPage
    {
        /*driver.Navigate().GoToUrl("http://asp.thedemosite.co.uk/");
        driver.Manage().Window.Size = new System.Drawing.Size(1175, 587);
        driver.FindElement(By.LinkText("4. Login")).Click();
        driver.FindElement(By.Name("username")).Click();
        driver.FindElement(By.Name("username")).SendKeys("user1");
        driver.FindElement(By.Name("password")).Click();
        driver.FindElement(By.Name("password")).SendKeys("1234");
        driver.FindElement(By.Name("FormsButton2")).Click();
        driver.FindElement(By.CssSelector("font > center")).Click();
        Assert.That(driver.FindElement(By.CssSelector("b")).Text, Is.EqualTo("**Failed Login**"));*/

        [FindsBy(How = How.Name, Using = "username")]
        private IWebElement nameElement;
        [FindsBy(How = How.Name, Using = "password")]
        private IWebElement passwordElement;
        [FindsBy(How = How.Name, Using = "FormsButton2")]
        private IWebElement loginElement;
        [FindsBy(How = How.CssSelector, Using = "b")]
        private IWebElement loginRes;
        public bool Displayed => nameElement.Displayed;
        public void TypeName(string name)
        {
            nameElement.SendKeys(name);
        }
        public void TypePassword(string password)
        {
            passwordElement.SendKeys(password);
        }
        public void ClickLogin()
        {
            loginElement.Click();
        }
        public void Login(string name, string password)
        {
            TypeName(name);
            TypePassword(password);
            ClickLogin();
        }

        public string LogginFailedMessge()
        {
            return "**Failed Login**";
        }

        public string LoginResult()
        { 
            return loginRes.Text;
        }

    }
    }