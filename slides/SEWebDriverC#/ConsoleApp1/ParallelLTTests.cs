﻿using System;
using System.Collections.Generic;
using System.Text;
    using OpenQA.Selenium;
    using OpenQA.Selenium.Remote;
    using NUnit.Framework;
    //using ParallelSelenium.Utils;
    using System.Threading;
using System.Linq;
using System.Threading.Tasks;
using System.Configuration;

namespace ParallelLTSelenium
{
    [TestFixture("chrome", "67.0", "Windows 10")]
    [TestFixture("internet explorer", "11.0", "Windows 7")]
    [TestFixture("firefox", "41.0", "Windows 7")]
    [TestFixture("internet explorer", "9.0", "Windows 7")]
    [TestFixture("firefox", "30.0", "Windows 7")]
    [TestFixture("internet explorer", "10.0", "Windows 7")]
    [TestFixture("firefox", "35.0", "Windows 7")]
    [Parallelizable(ParallelScope.Fixtures)]
    public class ParallelLTTests
    {

        ThreadLocal<IWebDriver> driver = new ThreadLocal<IWebDriver>();
        private String browser;
        private String version;
        private String os;
        private static String ltUserName;
        private static String ltAppKey;
        private  String platform;
        private  String browserVersion;

        public ParallelLTTests(String browser, String version, String os)
        {
            this.browser = browser;
            this.browserVersion = version;
            this.version = version;
            this.os = os;
            this.platform = os;
        }

        [SetUp]
        public void Init()
        {
        DesiredCapabilities caps1 = new DesiredCapabilities();
        caps1.SetCapability("platform", platform);
        caps1.SetCapability("browserName", browser); // name of your browser
        caps1.SetCapability("version", browserVersion); // version of your selected browser
        caps1.SetCapability("name", "CSharpTestSample");
        caps1.SetCapability("build", "LambdaTestSampleApp");
        caps1.SetCapability("user", Constants.Username);
        caps1.SetCapability("accessKey", Constants.AccessKey);
        caps1.SetCapability("network", true); // To enable network logs
        caps1.SetCapability("visual", true); // To enable step by step screenshot
        caps1.SetCapability("video", true); // To enable video recording
        caps1.SetCapability("console", true); // To capture console logs
        Console.WriteLine(ConfigurationSettings.AppSettings["LTUrl"]);
        driver.Value = new RemoteWebDriver(new Uri("https://hub.lambdatest.com/wd/hub"), caps1, TimeSpan.FromSeconds(600));
        driver.Value.Manage().Window.Maximize();
        }

        [Test]
        public void Todotest()
        {
            {
                driver.Value.Url = "https://lambdatest.github.io/sample-todo-app/";

                Assert.AreEqual("Sample page - lambdatest.com", driver.Value.Title);

                String itemName = "Yey, Let's add it to list";
                // Click on First Check box
                IWebElement firstCheckBox = driver.Value.FindElement(By.Name("li1"));
                firstCheckBox.Click();

                // Click on Second Check box
                IWebElement secondCheckBox = driver.Value.FindElement(By.Name("li2"));
                secondCheckBox.Click();

                // Enter Item name 
                IWebElement textfield = driver.Value.FindElement(By.Id("sampletodotext"));
                textfield.SendKeys(itemName);

                // Click on Add button
                IWebElement addButton = driver.Value.FindElement(By.Id("addbutton"));
                addButton.Click();

                // Verified Added Item name
                IWebElement itemtext = driver.Value.FindElement(By.XPath("/html/body/div/div/div/ul/li[6]/span"));
                String getText = itemtext.Text;
                Assert.IsTrue(itemName.Contains(getText));

            }
        }

        [TearDown]
        public void Cleanup()
        {
            bool passed = TestContext.CurrentContext.Result.Outcome.Status == NUnit.Framework.Interfaces.TestStatus.Passed;
            try
            {
                // Logs the result to Lambdatest
                ((IJavaScriptExecutor)driver.Value).ExecuteScript("lambda-status=" + (passed ? "passed" : "failed"));
            }
            finally
            {
                // Terminates the remote webdriver session
                driver.Value.Quit();
            }
        }
    }

    public static class Constants
    {
        internal static string Username = "jjorge";
        internal static string AccessKey = "28Ucdzik5XcLCtFMFfqRB2xHHXn2GBjFilDZ6BXS8nQ7ZBPooG";
        internal static Boolean tunnel = false;//Environment.GetEnvironmentVariable("TUNNEL");
        internal static string seleniumPort = Environment.GetEnvironmentVariable("SELENIUM_PORT");
        internal static string build = "C# Sample App";
        internal static string seleniumHost = Environment.GetEnvironmentVariable("SELENIUM_HOST");

    }
}